<?php
/*
 * Urtak Widget template 
 */
 global $base_url;
 $site_path = $base_url . base_path();
 $path = $site_path."node/".$nid;
 $created = format_date($created,'custom','F j, Y');
 ?>
  <script src="https://d39v39m55yawr.cloudfront.net/assets/clr.js" type='text/javascript'></script>
 <!-- URTAK EMBED CODE START -->
<div
  data-publication-key = '<?php print(variable_get('urtak_publication_key',"")); ?>'
  data-post-title           = "<?php print($title);?>"
  data-post-id              = "<?php print($nid);?>"
  data-post-permalink       = "<?php print($path);?>"
  data-post-created         = "<?php print($created);?>"
>
</div>
<!-- URTAK EMBED CODE END -->