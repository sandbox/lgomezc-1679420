<?php

/**
 * @file
 * Adds an urtak widget to nodes.
 */

 include_once dirname(__FILE__) . '/urtak-php/urtak_api.php';
 
 /**
 * implements hook_init()
 */
 function urtak_init(){
 	if((arg(0)=="node"&&arg(2)=='edit')){
 		drupal_add_css(drupal_get_path('module','urtak')."/css/urtak.css");
		drupal_add_js(drupal_get_path('module','urtak')."/js/urtak_questions.js");
		drupal_add_js(drupal_get_path('module','urtak')."/js/placeholder.min.js");
		drupal_add_js(drupal_get_path('module','urtak')."/js/raphael.js");
		drupal_add_js(drupal_get_path('module','urtak')."/js/pie.js");
		drupal_add_js(array('urtak' => array('nid' => arg(1))), 'setting');		
	}
 }

/**
 * Urtak is collaborative polling — everyone can ask questions. It's easy to engage a great number of people in a structured conversation that produces thousands of responses.
 */
 
// Constants to define the placement of the widget.
define('URTAK_NODE_DISPLAY_NO', 0); 
define('URTAK_NODE_DISPLAY_TEASER_ONLY', 1);
define('URTAK_NODE_DISPLAY_FULL_ONLY', 2);
define('URTAK_NODE_DISPLAY_BOTH', 3);


// Initialize new urtak api object
function urtak_api(){
	$publickey=variable_get('urtak_publication_key',"");
	$apikey=variable_get('urtak_api_key',"");
	$email=variable_get("urtak_email_user","");
	$configuration = array(
		'email'				=> $email,
		'publication_key' 	=> $publickey, 
		'api_key'			=> $apikey
	);
	return new Urtak($configuration);
}

/**
 * Implementation of hook_menu().
 */
function urtak_menu() {
  $items = array();
  $items['admin/settings/urtak'] = array(
    'title'            => 'Urtak Configuration',
    'description'      => 'Urtak Node settings',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('urtak_admin_settings'),
    'access arguments' => array('administer urtak'),
    'type'             => MENU_NORMAL_ITEM,
  );
  $items['urtak/update'] = array(
    'title' => 'Urtak questions administration', 
    'page callback' => 'urtak_update_question', 
    'access arguments' => array('administer urtak'), 
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Advanced menu settings callback.
 */
function urtak_admin_settings() {
  
  // Basic settings fieldset.
  $form['basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic settings'),
    '#group' => 'settings',
  );
  // Basic options
  $form['basic']['urtak_email_user'] = array(
    '#type' => 'textfield', 
    '#title' => t('Email address'),
    '#required'=>true,
    '#default_value'=>variable_get('urtak_email_user',""),
  );
  $form['basic']['urtak_api_key'] = array(
    '#type' => 'textfield', 
    '#title' => t('API Key'),
    '#description'=>t('this is like a password, do not share it'),
    '#default_value'=>variable_get('urtak_api_key',""),
  );
  $form['basic']['urtak_publication_key'] = array(
    '#type' => 'textfield', 
    '#title' => t('Publication Key'),
    '#default_value'=>variable_get('urtak_publication_key',""),
  );
  $form['urtak_node_types'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Node types'),
    '#description'   => t('Select the node types for which you want to activate urtak.'),
    '#default_value' => variable_get('urtak_node_types', array(NULL)),
    '#options'       => node_get_types('names'),
  );
  $form['urtak_widget_show'] = array(
    '#type'          => 'select',
    '#title'         => t('Widget display'),
    '#description'   => t('When will the urtak widget be displayed?'),
    '#default_value' => variable_get('urtak_widget_show', URTAK_NODE_DISPLAY_BOTH),
    '#options'       => array(URTAK_NODE_DISPLAY_TEASER_ONLY => 'Teaser only', URTAK_NODE_DISPLAY_FULL_ONLY => 'Full display only', URTAK_NODE_DISPLAY_BOTH => 'Both teaser and full'),
  );
  // Configuration.
  $form['configuration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuration'),
  );
  $form['configuration']['urtak_homepage'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Include on Homepage?'),
    '#description'=>t("Place Urtak with my homepage's article listing.<p>By default Urtak is included with your articles listed on the homepage. If you only want it to appear once they click through, uncheck this box.</p>"),
    '#default_value'=>variable_get('urtak_homepage',0),
  );
  $options = array();
  $options['en'] = 'English';
  $options['es'] = 'Español';
  $form['configuration']['urtak_language'] = array(
    '#type' => 'radios',
    '#title' => t('Language'),
    '#description' => t('Choose the language used in the widget your users see.'),
    '#default_value' => variable_get('urtak_language', 'en'),
    '#options' => $options,
  );
  $form['configuration']['urtak_bydefault'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Include Urtak by default?'),
    '#description'=>t("Place an Urtak on each of my posts.<p>Just like comments, Urtak is useful on every post and we'd love to see it there. We however do not think it is fair to have this enabled by default at this point and leave it up to you to determine. Urtak will only appear on posts where you've already asked a question first.</p>"),
    '#default_value'=>variable_get('urtak_bydefault',0),
  );
  $options = array();
  $options['community'] = 'Community (Automatic)';
  $options['publisher'] = 'Publisher (Manual)';
  $form['configuration']['urtak_community'] = array(
    '#type' => 'radios', 
    '#title' => t('Community moderation'),
    '#description'=>t("<p>We don't ever want Urtak to nag you. So, instead of requiring you to approve each and every question, we let the community determine whether or not a question gets removed through the \"Don't Care\" option. The more the button gets hit, the less frequently the question gets asked until it disappears entirely.</p>
                     <p>Click <a href=\"@learnmore\" target=\"_blank\">here to learn more</a> about community moderation. Of course, opting out of community moderation means you'll either receive emails for each question and/or can moderate through the post interface.</p>",
                      array(
                        '@learnmore' => 'https://urtak.com/faq#moderation',
                      )),
    '#default_value'=>variable_get('urtak_community',0),
    '#options' => $options,
  );
  $form['#validate'][] = 'urtak_admin_settings_validate';
  $form['#submit'][] = 'urtak_admin_settings_submit';
  return system_settings_form($form);
}

// Check if phpcurl is installed.
function urtak_admin_settings_validate($form, &$form_state) {
  // check to see that the API is accessible
  if ( ! function_exists( 'curl_init' ) || ! function_exists( 'curl_exec' ) ) {
    form_set_error('',t('PHP-CURL is not installed, please run apt-get install php5-curl'));
  }
}

// Save the urtak settings
function urtak_admin_settings_submit($form, &$form_state) {
   global $base_url;
   $publication_key=$form_state['values']['urtak_publication_key'];
   $api_key=$form_state['values']['urtak_api_key'];
   $email=$form_state['values']['urtak_email_user'];
   $publication_options = array(
      'domains'    => $base_url,
      'name'       => variable_get('site_name', ''),
      'platform'   => 'other',
      'moderation' => $form_state['values']['urtak_community'],
      'language'   => $form_state['values']['urtak_language'],
      'theme'      => 15
    );	
	// We need a publication key to identify this install and to provide security
    if ($publication_key == "") {
	  // We need only one thing to create an account:
      //    1. a valid token such as an email address
      if (($api_key == "") && $email != "") {
        $account_response = urtak_api()->create_account(array('email' => $email));
		// A successful response is 'created'
        // If the response was a failure, there are two likely reasons:
        //    1. server error
        //    2. the user already has an account! Gracefully fail, let them know they can sign in
        if($account_response->success()) {
          // Part of the response should be an API Key, so set that now.
          variable_set('urtak_api_key', $account_response->body['account']['api_key']);
        }
		else{
			drupal_set_message('You are already registered with Urtak!
Hello old friend, looks like you already have an account with us.
<a href="https://urtak.com/api_keys">Click here to sign in and get your keys.</a>',"error");
		}
      }
      // An API Key was either provided, create a publication automatically
      if ($api_key != "") {
        $publication_response = urtak_api()->create_publication('email', $email, $publication_options);
        // Great! lets store the key!
        if($publication_response->success()) {
          variable_set('urtak_publication_key', $publication_response->body['publication']['key']);
        }
      }

    // A publication key was provided! Update the publication with our current settings
    } else {
      $publication_response = urtak_api()->update_publication($publication_key, $publication_options);
	  if($publication_response->success()) {
         drupal_set_message(t("Success! You're done!"));
      }
    }
}

/**
 * Implementation of hook_form_alter
 * Embed the urtak field into the node form
 */
 
function urtak_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['type']) && isset($form['#node']) && $form['type']['#value'] . '_node_form' == $form_id) {
      $node_type = in_array($form['type']['#value'], variable_get('urtak_node_types', array()), TRUE);
      // if node type is selected to show urtak widgets.
      if ($node_type) {
      		$form['#cache'] = TRUE;
      		$form['urtak_wrapper'] = array(
      			'#type'=>'fieldset',
      			'#title'=>t('Ask questions with Urtak'),
		        '#tree' => FALSE,
			    '#prefix' => '<div class="clear-block" id="urtak-wrapper">',
			    '#suffix' => '</div>',
		    );
			  $node = $form['#node'];
			  if (isset($form_state['questions_count'])) {
			    $questions_count = $form_state['questions_count'];
			  }
			  else {
			    $questions_count = max(1, empty($node->urtak_question) ? 1 : count($node->urtak_question));
			  }
						  
				$response = urtak_api()->get_urtak_questions( 'post_id' , $node->nid , array());
				if($response->success() && $response->body['questions']['urtak']) {
					$preguntas = $response->body['questions']['question'];
					$urtakid = $response->body['questions']['urtak']['id'];
					$numquestions=count($preguntas);
				}
				$form['urtak_wrapper']['urtak_polls_ready'] = array(
			    '#prefix' => '<div id="urtak_recent_questions">',
			    '#suffix' => '</div>',
			    );
				for ($i = 0; $i < $numquestions; $i++) {
					$classes='';
					switch($preguntas[$i]['status']){
						case 'approved':
							$classes='<div class="urtak_approved urtak_on" data-action="approve"></div><div class="urtak_rejected urtak_off" data-action="reject"></div><div class="urtak_spam urtak_off" data-action="mark_as_spam"></div>';
						break;
						case 'rejected':
							$classes='<div class="urtak_approved urtak_off" data-action="approve"></div><div class="urtak_rejected urtak_on" data-action="reject"></div><div class="urtak_spam urtak_off" data-action="mark_as_spam"></div>';
						break;
						case 'spam':
							$classes='<div class="urtak_approved urtak_off" data-action="approve"></div><div class="urtak_rejected urtak_off" data-action="reject"></div><div class="urtak_spam urtak_on" data-action="mark_as_spam"></div>';
						break;
					}
					$responses=$preguntas[$i]['responses'];
					$info='<div class="urtak_pie" data-pie-count-care="'.$responses['counts']['care'].'" data-pie-percent-no="'.$responses['percents']['no'].'" data-pie-percent-yes="'.$responses['percents']['yes'].'"></div><table class="urtak_question_responses"><tbody><tr><th class="urtak_question_yes">Yes</th><td class="urtak_question_yes">'.$responses['counts']['yes'].'</td><th>Total</th><td>'.$responses['counts']['total'].'</td></tr><tr><th class="urtak_question_no">No</th><td class="urtak_question_no">'.$responses['counts']['no'].'</td><th>Care</th><td>'.$responses['percents']['care'].'%</td></tr></tbody></table><div class="urtak_question_links"><a href="https://urtak.com/u/'.$urtakid.'?question_id='.$preguntas[$i]['id'].'" target="_blank">Full Results »</a></div>';
					$form['urtak_wrapper']['urtak_polls_ready'][$i] = array(
				        '#type' => 'markup',
				        '#value' => $preguntas[$i]['text'],
				        '#prefix'=>'<div class="urtak_question" id="urtak-question-'.$preguntas[$i]['id'].'">'.$classes.'<span class="urtak_question_text">',
	        			'#suffix' => '</span><div class="urtak_question_info">'.$info.'</div></div>',
				      );
				}
			  $form['urtak_wrapper']['urtak_question'] = array(
			    '#prefix' => '<div id="urtak-questions">',
			    '#suffix' => '</div>',
			  );
			  // Add the current choices to the form.
			  for ($delta = 0; $delta < $questions_count; $delta++) {
			    
				$text = isset($node->urtak_question[$delta]['urtaktext']) ? $node->urtak_question[$delta]['urtaktext'] : '';
			    $form['urtak_wrapper']['urtak_question'][$delta] = _urtak_question_form($delta, $text);
			  	}
		    
			$form['urtak_wrapper']['urtak_widget_button'] = array(
		      '#type' => 'submit',
			    '#value' => t('Add more questions'),
			    '#description' => t(""),
			    '#weight' => 1,
			    '#submit' => array('urtak_more_questions_submit'), // If no javascript action.
			    '#ahah' => array(
			      'path' => 'urtak/js',
			      'wrapper' => 'urtak-questions',
			      'method' => 'replace',
			      'effect' => 'fade',
			    ),
		    );
  	  }
  }
}




/**
 * Implements hook_theme().
 */
function urtak_theme() {
  return array(
    'urtak_widget' => array(
      'arguments' => array('title'=>NULL,'nid'=>NULL,'created'=>NULL),
      'template' => 'urtak-widget',
    ),
  );
}


function urtak_questions_js() {
  include_once 'modules/node/node.pages.inc';
  $form_state = array(
    'storage' => NULL,
    'submitted' => FALSE,
  );
  $form_build_id = $_POST['form_build_id'];
  // Get the form from the cache.
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  // We will run some of the submit handlers so we need to disable redirecting.
  $form['#redirect'] = FALSE;
  // We need to process the form, prepare for that by setting a few internals
  // variables.
  $form['#post'] = $_POST;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;
  // Build, validate and if possible, submit the form.
  drupal_process_form($form_id, $form, $form_state);
  // This call recreates the form relying solely on the form_state that the
  // drupal_process_form set up.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  // Render the new output.
  $choice_form = $form['urtak_wrapper']['urtak_question'];
  unset($choice_form['#prefix'], $choice_form['#suffix']); // Prevent duplicate wrappers.
  $output = theme('status_messages') . drupal_render($choice_form);

  drupal_json(array('status' => TRUE, 'data' => $output));
}


/**
 * Submit handler to add more questions to a urtak form. This handler is used when
 * javascript is not available. It makes changes to the form state and the
 * entire form is rebuilt during the page reload.
 */
function urtak_more_questions_submit($form, &$form_state) {
  // Set the form to rebuild and run submit handlers.
  node_form_submit_build_node($form, $form_state);

  // Make the changes we want to the form state.
  if ($form_state['values']['urtak_widget_button']) {
    $n = $_GET['q'] == 'urtak/js' ? 1 : 5;
    $form_state['questions_count'] = count($form_state['values']['urtak_question']) + $n;
  }
}


function _urtak_question_form($delta, $value = '') {
  $form = array(
    '#tree' => TRUE,
  );

  // Manually set the #parents property of these fields so that
  // their values appear in the $form_state['values']['urtak_question'] array.
  $form['urtaktext'] = array(
    '#type' => 'textfield',
    '#title' => t('Question @n', array('@n' => ($delta + 1))),
    '#default_value' => $value,
    '#parents' => array('urtak_question', $delta, 'urtaktext'),
  );
  
  return $form;
}


/**
 * Implementation of hook_nodeapi().
 */
function urtak_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  	$exclude_modes = array(
        NODE_BUILD_PREVIEW,
        NODE_BUILD_SEARCH_INDEX,
        NODE_BUILD_SEARCH_RESULT,
        NODE_BUILD_RSS,
      );
      if (in_array($node->build_mode, $exclude_modes)) {
        break;
      }
	  $node_type = in_array($node->type, variable_get('urtak_node_types', array()), TRUE);
      $widget_showmode = variable_get('urtak_widget_show', URTAK_NODE_DISPLAY_BOTH);
		if ($node_type) {
			switch ($op) {
		    case 'view':
		      	  
				  	  $showinhome=variable_get('urtak_homepage',0);
			      	  $lookup = urtak_api()->get_urtak( 'post_id' , $node->nid , array() );
					  if($lookup->success()) {
				      	  // avoid showing the widget in some node builds
				      	  $show = false;
						  if(($showinhome==1&&drupal_is_front_page())){
						  	$show=true;
						  }
						  elseif(!drupal_is_front_page()){
						  	$show=true;
						  }
						  if($show){
					       	  switch ($widget_showmode) {
					            case URTAK_NODE_DISPLAY_TEASER_ONLY:
					              if ($teaser == 1) {
					              	
					                $node->content['urtak_widget_display'] = array(
					                  '#value' => theme('urtak_widget', $node->title, $node->nid, $node->created),
					                  '#weight' => 10,
					                );
					              }
					              break;
					            case URTAK_NODE_DISPLAY_FULL_ONLY:
					              if ($teaser == 0) {
					              	$node->content['urtak_widget_display'] = array(
					                  '#value' => theme('urtak_widget', $node->title, $node->nid, $node->created),
					                  '#weight' => 10,
					                );
					              }
					              break;
					            case URTAK_NODE_DISPLAY_BOTH:
					              $node->content['urtak_widget_display'] = array(
					                '#value' => theme('urtak_widget', $node->title, $node->nid, $node->created),
					                '#weight' => 10,
					              );
					              break;
					          }
				         }   
			          }
								
		      
		      break;
			  
			case 'insert':
			case 'update':
				//Update the urtak after insert the node
					$questions=array();
					foreach ($node->urtak_question as $key => $value) {
						if($value['urtaktext']!=""){
							$questions[]=array('text' => stripslashes($value['urtaktext']));
						}
					}
					if(count($questions)!=0||variable_get('urtak_bydefault',0)==1){
						// Don't trust the WP post meta, always do an API call to see if an Urtak already exists
				    	$lookup = urtak_api()->get_urtak( 'post_id' , $node->nid , array() );
						if($lookup->success()) {
						    
						    $urtak_id = $lookup->body['urtak']['id'];
						
						    $response = urtak_api()->create_urtak_questions('id', $urtak_id, $questions);
							if(!$response->success()) {
						    	drupal_set_message(t("The urtak questions for <em>@n</em> can not be updated.",array('@n'=>$node->title)),'error');
						    }
						 	// otherwise, create the Urtak and the questions
				  		}
						else{
							
							global $base_url;
							$site_path = $base_url . base_path();
						    $path = $site_path."node/".$node->nid;
						    $urtak = array(
						    	  'post_id'     => $node->nid,
						          'permalink'   => $path,
						          'title'       => $node->title,
						    );
							$response = urtak_api()->create_urtak($urtak, $questions);
							if(!$response->success()) {
						    	drupal_set_message(t("The urtak questions for <em>@n</em> can not be created.",array('@n'=>$node->title)),'error');
						    }
						}
					}
				
				break;
		}
  }
}

/**
 * 
 */
function urtak_update_question() {

	$questionid=explode("-",$_POST['question_id']);
	$question_id=$questionid[2];
    $post_id=$_POST['post_id'];
    $action=$_POST['status'];
	if($action == 'reject') {
    	$response = urtak_api()->reject_urtak_question( 'post_id' , $post_id , $question_id );
	} elseif($action == 'approve') {
	    $response = urtak_api()->approve_urtak_question( 'post_id' , $post_id , $question_id );
	} elseif($action == 'mark_as_spam') {
	    $response = urtak_api()->spam_urtak_question( 'post_id' , $post_id , $question_id );
	} elseif($action == 'mark_as_ham') {
	    $response = urtak_api()->ham_urtak_question( 'post_id' , $post_id , $question_id );
	}
	
	if($response->success()) {

    // handles an edge case : if you don't want urtak by default but enabled it 
    // by asking a question which you later un-approve, then make sure we check 
    // the approved questions count and reset the _show_urtak meta accordingly
    /*if (get_option('urtak_automatic_create') != 'true') {
      if ($action != 'approve') {
        $lookup = urtak_api()->get_urtak( 'post_id' , $post_id , array() );
        if ($lookup->body['urtak']['questions']['approved']['count'] == '0') {
          update_post_meta( $post_id, '_show_urtak' , '' );
        }
      } else {
        update_post_meta( $post_id, '_show_urtak' , 'show' );
      }
    }*/

    echo "success";
  } else {
    echo $response->error();
  }
	
	
}
