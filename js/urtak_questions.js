ajaxurl="/urtak/update";
jQuery(document).ready(function() {
  jQuery(".urtak_question").click( function(e){
    if(jQuery(e.target).is('.urtak_question')) {
      jQuery(this).toggleClass('urtak_question_hover');
      jQuery(this).children('.urtak_question_info').toggle('fast');
      return false;
    }
  });
  
  jQuery('.urtak_off').live("click",function(){
    var question = jQuery(this);
    post_id=Drupal.settings.urtak.nid;
    var data = {
      action: 'update_urtak_question',
      question_id: jQuery(this).parents('.urtak_question').attr('id'),
      post_id: post_id,
      status: jQuery(this).attr('data-action')
    };
   
    jQuery.post(ajaxurl, data, function(response) {
      if(response == 'success') {
        // if success, just update the icons
        question.removeClass('urtak_off');
        question.addClass('urtak_on');
        question.siblings('[data-action]').each(function() {
          jQuery(this).removeClass('urtak_on');
          jQuery(this).addClass('urtak_off');
        });
      } else {
        // return the error
        alert(response);
      }
    });
    
    return false;
  });
  $(".urtak_pie").pie(60);
});
